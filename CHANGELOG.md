# Changelog
All notable changes to this project will be documented in this file.

## 0.0.1
 - first release with only basic collections such as: linked list, stack and queue, plus binary search.