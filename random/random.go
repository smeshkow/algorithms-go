package random

import (
	"fmt"
	"math"
	"math/rand"
	"time"

	"bitbucket.org/smeshkow/algorithms-go/item"
)

// Item is an interface representing generic data transfer object.
type Item = item.Item

var (
	r = rand.New(rand.NewSource(time.Now().UnixNano()))
)

// Generate ...
func Generate(n int, lo, hi float64) {
	// check if provided bounds are integeres
	isInt := (lo == float64(int64(lo)) && hi == float64(int64(hi)))

	// generate and print n numbers between lo and hi
	for i := 0; i < n; i++ {
		if isInt {
			fmt.Printf("%d ", intRange(int64(lo), int64(hi)))
		} else {
			fmt.Printf("%.2f ", uniformFloatRange(lo, hi))
		}

		// new line after 50 numbers
		if i % 50 == 0 {
			print("\n")
		}
	}
}

func uniformFloatRange(a, b float64) float64 {
	if !(a < b) {
		panic(fmt.Sprintf("invalid range: [%.2f, %.2f)", a, b))
	}
	return a + r.Float64()*(b-a)
}

func intRange(a, b int64) int64 {
	if !(a < b) {
		panic(fmt.Sprintf("invalid range: [%d, %d)", a, b))
	}
	return int64(math.Max(float64(a), float64(r.Int63n(b))))
}

// Shuffle ...
func Shuffle(a item.Slice) {
	n := len(a)
    for i := 0; i < n; i++ {
        rn := i + uniform(n-i) // between i and n-1
        temp := a[i]
        a[i] = a[rn]
        a[rn] = temp
    }
}

// Returns a random integer uniformly in [0, n).
func uniform(n int) int {
	if n <= 0 {
		panic("argument must be positive")
	}
	return r.Intn(n)
}
