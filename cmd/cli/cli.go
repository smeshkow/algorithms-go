package main

import (
	"flag"
	"fmt"
	"os"

	"bitbucket.org/smeshkow/algorithms-go/graph"
	"bitbucket.org/smeshkow/algorithms-go/inout"
	"bitbucket.org/smeshkow/algorithms-go/item"
	"bitbucket.org/smeshkow/algorithms-go/random"
	"bitbucket.org/smeshkow/algorithms-go/search"
	"bitbucket.org/smeshkow/algorithms-go/sort"
)

var (
	// random command
	randomCmd     = flag.NewFlagSet("random", flag.ExitOnError)
	randomNumbers = randomCmd.Int("n", 0, "Number of numbers to generate, e.g. -n=30")
	randomLo      = randomCmd.Float64("lo", 0.0, "Minimum number in the generated range, e.g. -lo=1")
	randomHi      = randomCmd.Float64("hi", 0.0, "Maximum number in the generated range, e.g. -hi=100")

	// search command
	searchCmd  = flag.NewFlagSet("search", flag.ExitOnError)
	searchType = searchCmd.String("t", "binary", "Type of the search, e.g. -t=binary")
	searchIn   = searchCmd.String("in", "", "Search from file or web URL, e.g. -in=/tmp/example.txt")
	searchKey  = searchCmd.Int("k", 0, "Search for key, e.g. -k=7")

	// sort command
	sortCmd  = flag.NewFlagSet("sort", flag.ExitOnError)
	sortType = sortCmd.String("t", "quick", "Type of the sort, e.g. -t=quick")
	sortIn   = sortCmd.String("in", "", "Sort from file or web URL, e.g. -in=/tmp/example.txt")

	// graph command
	graphCmd  = flag.NewFlagSet("graph", flag.ExitOnError)
	graphType = graphCmd.String("t", "dfs", "Type of Graph operation, e.g -t=dfs")
	graphIn   = graphCmd.String("in", "", "Look in file or web URL, e.g. -in=/tmp/example.txt")
	graphS    = graphCmd.Int("s", 0, "Source / root to search from")
)

func main() {
	// Verify that a subcommand has been provided
	// os.Arg[0] is the main command
	// os.Arg[1] will be the subcommand
	if len(os.Args) < 2 {
		println("sub command is required")
		os.Exit(1)
	}

	cmd := os.Args[1]

	switch cmd {
	case randomCmd.Name():
		parseCommand(randomCmd)
		random.Generate(*randomNumbers, *randomLo, *randomHi)
	case searchCmd.Name():
		parseCommand(searchCmd)
		doSearch()
	case sortCmd.Name():
		parseCommand(sortCmd)
		doSort()
	case graphCmd.Name():
		parseCommand(graphCmd)
		doGraph()
	default:
		fmt.Printf("unknown sub command: %s\n", cmd)
		os.Exit(2)
	}
}

func parseCommand(cmd *flag.FlagSet) {
	err := cmd.Parse(os.Args[2:])
	if err != nil {
		fmt.Printf("error in parsing arguments: %v \n", err)
		cmd.PrintDefaults()
		os.Exit(3)
	}
}

func readStrings(name string) []string {
	in := inout.NewIn(name)
	defer in.Close()
	return in.ReadAllStrings()
}

func readItems(name string) []item.Item {
	return item.FromStrings(readStrings(name))
}

func doSearch() {
	switch *searchType {
	case "binary":
		search.BinarySearch(*searchIn, *searchKey)
	case "binary-recursive":
		search.BinarySearchRecursive(*searchIn, *searchKey)
	default:
		fmt.Printf("unknown search type: %s\n", *searchType)
		searchCmd.PrintDefaults()
		os.Exit(4)
	}
}

func doSort() {
	switch *sortType {
	case "quick":
		sort.Quick(readItems(*sortIn))
	default:
		fmt.Printf("unknown sort type: %s\n", *sortType)
		sortCmd.PrintDefaults()
		os.Exit(5)
	}
}

func doGraph() {
	var g graph.Graph

	if *graphIn != "" {
		g = graph.NewFromIn(inout.NewIn(*graphIn))
	} else {
		fmt.Printf("not enough information to initialize graph: %s\n", *graphType)
		graphCmd.PrintDefaults()
		os.Exit(6)
	}

	switch *graphType {
	case "dfs":
		if *graphS == -1 {
			fmt.Printf("source vertex to search from must be greater or equal to 0 (option -s): %s\n", *graphType)
			graphCmd.PrintDefaults()
			os.Exit(7)
		}

		dfs := graph.NewDFS(g, *graphS)
		if dfs.Count() != g.V() {
			println("NOT connected")
		} else {
			println("connected")
		}
	default:
		fmt.Printf("unknown graph operation type: %s\n", *graphType)
		graphCmd.PrintDefaults()
		os.Exit(8)
	}
}
