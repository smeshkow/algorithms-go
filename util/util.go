package util

import (
	"fmt"
	"strings"
)

// Must ...
func Must(err error, msg ...string) {
	if err != nil {
		panic(fmt.Sprintf("%s: %v", strings.Join(msg, ": "), err))
	}
}
