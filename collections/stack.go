package collections

// Stack - pushdown last-in-first-out (LIFO) stack.
type Stack interface {
	Push(item Item)
	Pop() Item
	IsEmpty() bool
	Size() int
}

// SliceStack ...
type SliceStack struct {
	s []Item // slice that holds elements of the stack
	n int    // number of elements on stack
	c int    // capacity of the stack, -1 means unlimited
}

// NewSliceStack ...
func NewSliceStack() *SliceStack {
	return &SliceStack{
		s: make([]Item, 0),
		n: 0,
		c: unlimited,
	}
}

// NewFixedSliceStack ...
func NewFixedSliceStack(capacity int) *SliceStack {
	return &SliceStack{
		s: make([]Item, 0),
		n: 0,
		c: capacity,
	}
}

// Push adds the item to this stack.
func (ss *SliceStack) Push(item Item) {
	if ss.IsFull() {
		ss.s = append(ss.s[1:], item) // wrap arround
		return
	}
	ss.s = append(ss.s, item)
	ss.n++
}

// Pop removes and returns the item on this stack that was most recently added.
func (ss *SliceStack) Pop() Item {
	if ss.IsEmpty() {
		return nil
	}
	var item Item
	// pop ...
	item, ss.s = ss.s[ss.n - 1], ss.s[:ss.n - 1]
	ss.n--
	return item
}

// IsEmpty ...
func (ss *SliceStack) IsEmpty() bool {
	return ss.n == 0
}

// IsFull ...
func (ss *SliceStack) IsFull() bool {
	return ss.n == ss.c
}

// Size ...
func (ss *SliceStack) Size() int {
	return ss.n
}

// LinkedListStack is pushdown last-in-first-out (LIFO) implementation of stack based on linked list.
type LinkedListStack struct {
	first *node
	size  int
}

// NewLinkedListStack ...
func NewLinkedListStack() *LinkedListStack {
	return &LinkedListStack{}
}

// Push ...
func (s *LinkedListStack) Push(item Item) {
	s.first = &node{
		value: item,
		next:  s.first,
	}
	s.size++
}

// Pop ...
func (s *LinkedListStack) Pop() Item {
	if s.IsEmpty() {
		return nil
	}
	oldFirst := s.first
	s.first = s.first.next
	s.size--
	return oldFirst.value
}

// IsEmpty ...
func (s *LinkedListStack) IsEmpty() bool {
	return s.first == nil
}

// Size ...
func (s *LinkedListStack) Size() int {
	return s.size
}
