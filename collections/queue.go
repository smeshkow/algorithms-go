package collections

// Queue ...
type Queue interface {
	Enqueue(item Item)
	Denqueue() Item
	IsEmpty() bool
	Size() int
}

// SliceQueue ...
type SliceQueue struct {
	q Slice // queue elements
	n int    // number of elements on queue
	c int // capacity of the queue, -1 means unlimited
}

// NewSliceQueue initializes an empty queue.
func NewSliceQueue() *SliceQueue {
	return &SliceQueue{
		q: make(Slice, 0),
		n: 0,
		c: unlimited,
	}
}

// NewFixedSliceQueue initializes an empty queue.
func NewFixedSliceQueue(capacity int) *SliceQueue {
	return &SliceQueue{
		q: make(Slice, 0, capacity),
		n: 0,
		c: capacity,
	}
}

// IsEmpty - is this queue empty? Returns true if this queue is empty; false otherwise.
func (sq *SliceQueue) IsEmpty() bool {
	return sq.n == 0
}

// IsFull - is this queue full? Returns true if this queue is full; false otherwise.
func (sq *SliceQueue) IsFull() bool {
	return sq.n == sq.c
}

// Size returns the number of items in this queue.
func (sq *SliceQueue) Size() int {
	return sq.n
}

// Enqueue adds the item to this queue.
func (sq *SliceQueue) Enqueue(item Item) {
	if sq.c != unlimited && sq.c == len(sq.q) {
		sq.q = append(sq.q[1:], item) // wrap arround
		return
	}
	sq.q = append(sq.q, item)
	sq.n++
}

// Denqueue removes and returns the item on this queue that was least recently added.
func (sq *SliceQueue) Denqueue() Item {
	if sq.IsEmpty() {
		return nil
	}
	var item Item
	// pop ...
	item, sq.q = sq.q[0], sq.q[1:]
	sq.n--
	return item
}

// LinkedListQueue is first-in-first-out (FIFO) implementation of stack based on linked list.
type LinkedListQueue struct {
	first *node
	last  *node
	size  int
}

// NewLinkedListQueue ...
func NewLinkedListQueue() *LinkedListQueue {
	return &LinkedListQueue{
		first: nil,
		last:  nil,
		size:  0,
	}
}

// Enqueue ...
func (llq *LinkedListQueue) Enqueue(item Item) {
	llq.size++
	if llq.IsEmpty() {
		llq.first = &node{value: item}
		llq.last = llq.first
		return
	}
	oldLast := llq.last
	llq.last = &node{
		value: item,
	}
	oldLast.next = llq.last
}

// Denqueue ...
func (llq *LinkedListQueue) Denqueue() Item {
	if llq.IsEmpty() {
		return nil
	}
	oldFirst := llq.first
	llq.first = llq.first.next
	return oldFirst.value
}

// IsEmpty ...
func (llq *LinkedListQueue) IsEmpty() bool {
	return llq.first == nil
}

// Size ...
func (llq *LinkedListQueue) Size() int {
	return llq.size
}
