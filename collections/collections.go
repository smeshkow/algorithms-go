package collections

import (
	"bitbucket.org/smeshkow/algorithms-go/item"
)

// Item is an interface representing generic data transfer object.
type Item = item.Item

// Slice represents slice of instances of Item.
type Slice = item.Slice

// if collection's capacity is -1 the it is unlimited.
const unlimited = -1

// Consumer ...
type Consumer func(item Item)

// Iterator ...
type Iterator interface {
	HasNext() bool      // returns true if there is next element.
	Next() Item         // returns next element.
	Iterate(c Consumer) // applies given Consumer to each element of iterator
}

// Iterable ...
type Iterable interface {
	Iterator() Iterator // returns iterator over this Iterable.
}

// Rangeable ...
type Rangeable interface {
	Slice() Slice // returns slice of this Rangeable to range over it.
}
