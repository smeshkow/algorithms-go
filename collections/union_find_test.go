package collections

import (
	"testing"
)

func TestFind(t *testing.T)  {
	uf := NewUF(10)

	if uf.Find(1) != 1 {
		t.Error("expected 1")
	}
}

func TestUnion(t *testing.T)  {
	uf := NewUF(10)

	if uf.Find(1) != 1 {
		t.Errorf("expected 1 to point to 1, but actual %d", uf.Find(1))
	}

	if uf.Find(2) != 2 {
		t.Errorf("expected 2 to point to 2, but actual %d", uf.Find(2))
	}

	uf.Union(1, 2)

	if uf.Find(2) != 1 {
		t.Errorf("expected 2 to point to 1, but actual %d", uf.Find(2))
	}
}

func TestConnected(t *testing.T)  {
	uf := NewUF(10)

	if uf.Connected(1, 2) {
		t.Error("expected 1 and 2 to be not connected")
	}

	uf.Union(1, 2)

	if !uf.Connected(1, 2) {
		t.Error("expected 1 and 2 to be connected")
	}
}

func TestCount(t *testing.T)  {
	uf := NewUF(10)

	if uf.Count() != 10 {
		t.Errorf("expected count is 10, but actual is %d", uf.Count())
	}

	uf.Union(1, 2)

	if uf.Count() != 9 {
		t.Errorf("expected count is 9, but actual is %d", uf.Count())
	}
}