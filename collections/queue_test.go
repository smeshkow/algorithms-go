package collections

import (
	"testing"
	
	"bitbucket.org/smeshkow/algorithms-go/item"
)

func TestSliceQueue_Size(t *testing.T)  {
	queue := NewSliceQueue()
	if queue.Size() != 0 {
		t.Error("expected size is 0")
	}
	queue.Enqueue(item.New())
	if queue.Size() != 1 {
		t.Error("expected size is 1")
	}
}

func TestSliceQueue_IsEmpty(t *testing.T)  {
	queue := NewSliceQueue()
	if !queue.IsEmpty() {
		t.Error("expected to be empty")
	}
	queue.Enqueue(item.New())
	if queue.IsEmpty() {
		t.Error("expected not to be empty")
	}
}

func TestSliceQueue_Enqueue(t *testing.T)  {
	queue := NewSliceQueue()
	queue.Enqueue(item.NewInt(1))
	it := queue.Denqueue()
	if it == nil {
		t.Error("expected item to be not nil")
	}
	if it.GetInt() != 1 {
		t.Error("expected item's int value to be 1")
	}
}

func TestSliceQueue_Denqueue(t *testing.T)  {
	queue := NewSliceQueue()
	
	queue.Enqueue(item.NewInt(1))
	queue.Enqueue(item.NewInt(2))

	it := queue.Denqueue()
	if it == nil {
		t.Error("expected item to be not nil")
	}
	if it.GetInt() != 1 {
		t.Error("expected item's int value to be 1")
	}

	it = queue.Denqueue()
	if it == nil {
		t.Error("expected item to be not nil")
	}
	if it.GetInt() != 2 {
		t.Error("expected item's int value to be 2")
	}

	if !queue.IsEmpty() {
		t.Error("expected to be empty")
	}
}

func TestFixedSliceQueue_Enqueue(t *testing.T)  {
	queue := NewFixedSliceQueue(3)

	queue.Enqueue(item.NewInt(1))
	queue.Enqueue(item.NewInt(2))
	queue.Enqueue(item.NewInt(3))
	queue.Enqueue(item.NewInt(4))

	if queue.Size() != 3 {
		t.Error("expected size to be 3")
	}

	itemVal := queue.Denqueue().GetInt()
	if itemVal != 2 {
		t.Errorf("expected item's int value to be 2, but actual %d", itemVal)
	}
}

func TestFixedSliceQueue_IsFull(t *testing.T)  {
	queue := NewFixedSliceQueue(1)

	queue.Enqueue(item.NewInt(1))

	if queue.Size() != 1 {
		t.Error("expected size to be 1")
	}
	if !queue.IsFull() {
		t.Errorf("expected queue to be full")
	}
}

func TestLinkedListQueue_Size(t *testing.T)  {
	queue := NewLinkedListQueue()
	if queue.Size() != 0 {
		t.Error("expected size is 0")
	}
	queue.Enqueue(item.New())
	if queue.Size() != 1 {
		t.Error("expected size is 1")
	}
}

func TestLinkedListQueue_IsEmpty(t *testing.T)  {
	queue := NewLinkedListQueue()
	if !queue.IsEmpty() {
		t.Error("expected to be empty")
	}
	queue.Enqueue(item.New())
	if queue.IsEmpty() {
		t.Error("expected not to be empty")
	}
}

func TestLinkedListQueue_Enqueue(t *testing.T)  {
	queue := NewLinkedListQueue()
	queue.Enqueue(item.NewInt(1))
	it := queue.Denqueue()
	if it == nil {
		t.Error("expected item to be not nil")
	}
	if it.GetInt() != 1 {
		t.Error("expected item's int value to be 1")
	}
}

func TestLinkedListQueue_Denqueue(t *testing.T)  {
	queue := NewLinkedListQueue()

	queue.Enqueue(item.NewInt(1))
	queue.Enqueue(item.NewInt(2))

	it := queue.Denqueue()
	if it == nil {
		t.Error("expected item to be not nil")
	}
	if it.GetInt() != 1 {
		t.Error("expected item's int value to be 1")
	}

	it = queue.Denqueue()
	if it == nil {
		t.Error("expected item to be not nil")
	}
	if it.GetInt() != 2 {
		t.Error("expected item's int value to be 2")
	}

	if !queue.IsEmpty() {
		t.Error("expected to be empty")
	}
}