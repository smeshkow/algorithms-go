package collections

// Bag ...
type Bag interface {
	Add(item Item)
	IsEmpty() bool
	Size() int
	Iterable
}

// SliceBag ...
type SliceBag struct {
	b Slice
	n int // size of the bag
	c int // capacity, -1 - unlimited
}

// LinkedListBag ...
type LinkedListBag struct {
	first *node
	n     int // size
}

type linkedListBagIterator struct {
	cur *node
}

// HasNext returns true if there is next element
func (it *linkedListBagIterator) HasNext() bool {
	return it.cur != nil
}

// Next returns next element
func (it *linkedListBagIterator) Next() Item {
	v := it.cur.value
	it.cur = it.cur.next
	return v
}

func (it *linkedListBagIterator) Iterate(c Consumer) {
	for it.HasNext() {
		c(it.Next())
	}
}

// NewSliceBag ...
func NewSliceBag() *SliceBag {
	return &SliceBag{
		b: make(Slice, 0),
		n: 0,
		c: unlimited,
	}
}

// NewFixedSliceBag ...
func NewFixedSliceBag(capacity int) *SliceBag {
	return &SliceBag{
		b: make(Slice, 0, capacity),
		n: 0,
		c: capacity,
	}
}

// Add ...
func (sb *SliceBag) Add(item Item) {
	if sb.c != unlimited && sb.c == len(sb.b) {
		sb.b = append(sb.b[1:], item) // wrap arround
		return
	}
	sb.b = append(sb.b, item)
	sb.n++
}

// IsEmpty ...
func (sb *SliceBag) IsEmpty() bool {
	return sb.n == 0
}

// IsFull ...
func (sb *SliceBag) IsFull() bool {
	return sb.n == sb.c
}

// Size ...
func (sb *SliceBag) Size() int {
	return sb.n
}

// NewLinkedListBag ...
func NewLinkedListBag() *LinkedListBag {
	return &LinkedListBag{}
}

// IsEmpty ...
func (b *LinkedListBag) IsEmpty() bool {
	return b.n == 0
}

// Size ...
func (b *LinkedListBag) Size() int {
	return b.n
}

// Add ...
func (b *LinkedListBag) Add(item Item) {
	b.first = &node{
		value: item,
		next:  b.first,
	}
	//b.it.cur = b.first
	b.n++
}

// Iterator ...
func (b *LinkedListBag) Iterator() Iterator {
	return &linkedListBagIterator{cur: b.first}
}
