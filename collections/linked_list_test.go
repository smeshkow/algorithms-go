package collections

import (
	"testing"
	
	"bitbucket.org/smeshkow/algorithms-go/item"
)

func TestLinkedList_Size(t *testing.T) {
	list := NewLinkedList()
	if list.Size() != 0 {
		t.Error("expected size is 0")
	}
	list.Add(item.New())
	if list.Size() != 1 {
		t.Error("expected size is 1")
	}
}

func TestLinkedList_IsEmpty(t *testing.T) {
	list := NewLinkedList()
	if !list.IsEmpty() {
		t.Error("expected to be empty")
	}
	list.Add(item.New())
	if list.IsEmpty() {
		t.Error("expected not to be empty")
	}
}

func TestLinkedList_Add(t *testing.T) {
	list := NewLinkedList()
	list.Add(item.NewInt(1))
	it := list.Get(0)
	if it == nil {
		t.Error("expected item to be not nil")
	}
	if it.GetInt() != 1 {
		t.Error("expected item's int value to be 1")
	}
}

func TestLinkedList_Get(t *testing.T) {
	list := NewLinkedList()
	
	list.Add(item.NewInt(1))
	list.Add(item.NewInt(2))

	item := list.Get(1)
	if item == nil {
		t.Error("expected item to be not nil")
	}
	if item.GetInt() != 2 {
		t.Error("expected item's int value to be 2")
	}
}

func TestLinkedList_Remove(t *testing.T) {
	list := NewLinkedList()
	
	item1 := item.NewInt(1)
	item2 := item.NewInt(2)
	item3 := item.NewInt(3)

	list.Add(item1)
	list.Add(item2)
	list.Add(item3)

	if list.Size() != 3 {
		t.Error("expected size to be 3")
	}

	list.Remove(item2)

	if list.Size() != 2 {
		t.Error("expected size to be 2")
	}
}

func TestLinkedList_Remove_First(t *testing.T) {
	list := NewLinkedList()

	item1 := item.NewInt(1)
	item2 := item.NewInt(2)
	item3 := item.NewInt(3)

	list.Add(item1)
	list.Add(item2)
	list.Add(item3)

	if list.Size() != 3 {
		t.Error("expected size to be 3")
	}

	list.Remove(item1)

	if list.Size() != 2 {
		t.Error("expected size to be 2")
	}
}

func TestLinkedList_Last(t *testing.T) {
	list := NewLinkedList()

	item1 := item.NewInt(1)
	item2 := item.NewInt(2)
	item3 := item.NewInt(3)

	list.Add(item1)
	list.Add(item2)
	list.Add(item3)

	if list.Size() != 3 {
		t.Error("expected size to be 3")
	}

	list.Remove(item3)

	if list.Size() != 2 {
		t.Error("expected size to be 2")
	}
}

func TestLinkedList_Single(t *testing.T) {
	list := NewLinkedList()

	item1 := item.NewInt(1)

	list.Add(item1)

	if list.Size() != 1 {
		t.Error("expected size to be 3")
	}

	list.Remove(item1)

	if !list.IsEmpty() {
		t.Error("expected list to be empty")
	}
}

func TestLinkedList_Empty(t *testing.T) {
	list := NewLinkedList()

	if !list.IsEmpty() {
		t.Error("expected list to be empty")
	}

	list.Add(item.NewInt(1))

	if list.IsEmpty() {
		t.Error("expected list to be not empty")
	}
}

