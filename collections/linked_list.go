package collections

// List ...
type List interface {
	Add(item Item)
	Get(index int) Item
	Remove(item Item)
	IsEmpty() bool
	Size() int
}

// node ...
type node struct {
	value Item
	next  *node
}

// LinkedList ...
type LinkedList struct {
	first *node
	last  *node
	size  int
}

// NewLinkedList ...
func NewLinkedList() *LinkedList {
	return &LinkedList{}
}

// Add ...
func (l *LinkedList) Add(item Item) {
	newEntry := &node{value: item}
	if l.IsEmpty() {
		l.first = newEntry
		l.size++
		return
	}

	oldLast := l.last
	l.last = newEntry
	if oldLast == nil {
		l.first.next = l.last
	} else {
		oldLast.next = l.last
	}
	l.size++
}

// Get ...
func (l *LinkedList) Get(index int) Item {
	if l.IsEmpty() || index >= l.size {
		return nil
	}

	if index == 0 {
		return l.first.value
	}

	if index == l.size - 1 {
		return l.last.value
	}

	entry := l.first
	for i := 1; i <= index; i++ {
		entry = entry.next
	}
	return entry.value
}

// Remove ...
func (l *LinkedList) Remove(item Item) {
	if l.IsEmpty() {
		return
	}

	// iterate in order to find matching entry
	var prev, current *node
	current = l.first

	for current != nil {
		if current.value.Equals(item) {
			// First element
			if prev == nil {
				l.first = current.next
			// Others
			} else {
				if l.last != nil && current.value.Equals(l.last.value) {
					l.last = prev
				}
				prev.next = current.next
			}
			l.size--
			return
		}

		prev = current
		current = current.next
	}

}

// IsEmpty ...
func (l *LinkedList) IsEmpty() bool {
	return l.first == nil
}

// Size ...
func (l *LinkedList) Size() int {
	return l.size
}
