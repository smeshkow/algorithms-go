package collections

import (
	"testing"

	"bitbucket.org/smeshkow/algorithms-go/item"
)

func TestLinkedListStack_Size(t *testing.T)  {
	stack := NewLinkedListStack()
	if stack.Size() != 0 {
		t.Error("expected size is 0")
	}
	stack.Push(item.New())
	if stack.Size() != 1 {
		t.Error("expected size is 1")
	}
}

func TestLinkedListStack_IsEmpty(t *testing.T)  {
	stack := NewLinkedListStack()
	if !stack.IsEmpty() {
		t.Error("expected to be empty")
	}
	stack.Push(item.New())
	if stack.IsEmpty() {
		t.Error("expected not to be empty")
	}
}

func TestLinkedListStack_Push(t *testing.T)  {
	stack := NewLinkedListStack()
	stack.Push(item.NewInt(1))
	it := stack.Pop()
	if it == nil {
		t.Error("expected item to be not nil")
	}
	if it.GetInt() != 1 {
		t.Error("expected item's int value to be 1")
	}
}

func TestLinkedListStack_Pop(t *testing.T)  {
	stack := NewLinkedListStack()
	
	stack.Push(item.NewInt(1))
	stack.Push(item.NewInt(2))

	it := stack.Pop()
	if it == nil {
		t.Error("expected item to be not nil")
	}
	if it.GetInt() != 2 {
		t.Error("expected item's int value to be 2")
	}

	it = stack.Pop()
	if it == nil {
		t.Error("expected item to be not nil")
	}
	if it.GetInt() != 1 {
		t.Error("expected item's int value to be 1")
	}

	if !stack.IsEmpty() {
		t.Error("expected to be empty")
	}
}