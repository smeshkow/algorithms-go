package collections

import "fmt"

// UF is a Union Find API
type UF interface {
	Union(p, q int)          // add connection between p and q
	Find(p int) int          // component identifier for p (0 - N-1)
	Connected(p, q int) bool // return true if p and q are in the sme component
	Count() int              // number of components
}

// WeightedQuickUnionFind ...
type WeightedQuickUnionFind struct {
	parent []int  // parent[i] = parent of i
	rank   []byte // rank[i] = rank of subtree rooted at i (never more than 31)
	count  int    // number of components
}

// NewUF initializes an empty union–find data structure with n sites
// 0 through n-1. Each site is initially in its own component.
func NewUF(n int) UF {
	if n < 0 {
		panic("n < 0")
	}

	count := n
	parent := make([]int, n)
	rank := make([]byte, n)
	for i := 0; i < n; i++ {
		parent[i] = i
		rank[i] = 0
	}

	return &WeightedQuickUnionFind{
		parent: parent,
		rank:   rank,
		count:  count,
	}
}

// Find returns the component identifier for the component containing site p.
func (uf *WeightedQuickUnionFind) Find(p int) int {
	uf.validate(p)
	for p != uf.parent[p] {
		uf.parent[p] = uf.parent[uf.parent[p]] // path compression by halving
		p = uf.parent[p]
	}
	return p
}

// Union merges the component containing site p with 
// the component containing site q.
func (uf *WeightedQuickUnionFind) Union(p, q int) {
	rootP := uf.Find(p)
	rootQ := uf.Find(q)
	if rootP == rootQ {
		return
	}

	// make root of smaller rank point to root of larger rank
	if uf.rank[rootP] < uf.rank[rootQ] {
		uf.parent[rootP] = rootQ
	} else if uf.rank[rootP] > uf.rank[rootQ] {
		uf.parent[rootQ] = rootP
	} else {
		uf.parent[rootQ] = rootP
		uf.rank[rootP]++
	}
	uf.count--
}

// Connected returns true if two sites are in the same component.
func (uf *WeightedQuickUnionFind) Connected(p, q int) bool {
	return uf.Find(p) == uf.Find(q)
}

// Count returns the number of components.
func (uf *WeightedQuickUnionFind) Count() int {
	return uf.count
}

// validate that p is a valid index
func (uf *WeightedQuickUnionFind) validate(p int) {
	n := len(uf.parent)
	if p < 0 || p >= n {
		panic(fmt.Sprintf("index %d is not between 0 and %d", p, n-1))
	}
}
