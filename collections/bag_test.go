package collections

import (
	"testing"

	"bitbucket.org/smeshkow/algorithms-go/item"
)

func TestSliceBag_Size(t *testing.T)  {
	bag := NewSliceBag()
	if bag.Size() != 0 {
		t.Error("expected size is 0")
	}
	bag.Add(item.New())
	if bag.Size() != 1 {
		t.Error("expected size is 1")
	}
}

func TestSliceBag_IsEmpty(t *testing.T)  {
	bag := NewSliceBag()
	if !bag.IsEmpty() {
		t.Error("expected to be empty")
	}
	bag.Add(item.New())
	if bag.IsEmpty() {
		t.Error("expected not to be empty")
	}
}

func TestSliceBag_IsFull(t *testing.T)  {
	bag := NewFixedSliceBag(1)
	if bag.IsFull() {
		t.Error("expected to not be full")
	}
	bag.Add(item.New())
	if !bag.IsFull() {
		t.Error("expected to be full")
	}
}