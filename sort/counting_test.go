package sort

import (
	"testing"

	"bitbucket.org/smeshkow/algorithms-go/item"
)

func TestCounting_1(t *testing.T) {
	max := 9 // range of 0 ... 9
	input := []int{1, 4, 1, 2, 7, 5, 2}
	expected := []int{1, 1, 2, 2, 4, 5, 7}
	Counting(input, max)
	if !item.FromInts(expected).Equals(item.FromInts(input)) {
		t.Errorf("expected order to be %v, but actual %v", expected, input)
	}
}

func TestCounting_2(t *testing.T) {
	max := 9 // range of 0 ... 9
	input := []int{1, 4, 1, 2, 0, 7, 5, 0, 2}
	expected := []int{0, 0, 1, 1, 2, 2, 4, 5, 7}
	Counting(input, max)
	if !item.FromInts(expected).Equals(item.FromInts(input)) {
		t.Errorf("expected order to be %v, but actual %v", expected, input)
	}
}