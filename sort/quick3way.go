package sort

import (
	"bitbucket.org/smeshkow/algorithms-go/random"
)

// Quick3Way provides methods for sorting an
// array using quicksort with 3-way partitioning.
//
// For additional documentation, see <a href="https://algs4.cs.princeton.edu/21elementary">Section 2.1</a> of
// Algorithms, 4th Edition by Robert Sedgewick and Kevin Wayne.
func Quick3Way(a Slice) {
	random.Shuffle(a)
	quickSort3way(a, 0, len(a) - 1)
}

// quicksort the subarray a[lo .. hi] using 3-way partitioning
func quickSort3way(a Slice, lo, hi int) {
	if hi <= lo {
		return
	}
	lt := lo
	gt := hi
	v := a[lo]
	i := lo + 1
	for i <= gt {
		cmp := a[i].(Comparable).Compare(v)
		if cmp < 0 {
			exch(a, lt, i)
			lt++
			i++
		} else if cmp > 0 {
			exch(a, i, gt)
			gt--
		} else {
			i++
		}
	}

	// a[lo..lt-1] < v = a[lt..gt] < a[gt+1..hi].
	quickSort3way(a, lo, lt-1)
	quickSort3way(a, gt+1, hi)
}
