package sort

// Merge ...
func Merge(a Slice)  {
	aux := make(Slice, len(a))
    mergeSort(a, aux, 0, len(a)-1)
}

// mergeSort a[lo..hi] using auxiliary array aux[lo..hi]
func mergeSort(a, aux Slice, lo, hi int) {
	if hi <= lo {
		return
	}
	mid := lo + (hi - lo) / 2
	mergeSort(a, aux, lo, mid)
	mergeSort(a, aux, mid + 1, hi)
	merge(a, aux, lo, mid, hi)
}

// stably merge a[lo .. mid] with a[mid+1 ..hi] using aux[lo .. hi]
func merge(a, aux Slice, lo, mid, hi int) {
	// copy to aux[]
	for k := lo; k <= hi; k++ {
		aux[k] = a[k]
	}

	// merge back to a[]
	i := lo 
	j := mid+1
	for k := lo; k <= hi; k++ {
		if i > mid {
			a[k] = aux[j]
			j++
		} else if j > hi { 
			a[k] = aux[i]
			i++
		} else if less(aux[j], aux[i]) { 
			a[k] = aux[j]
			j++
		} else {
			a[k] = aux[i]
			i++
		}
	}
}