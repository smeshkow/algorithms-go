package sort

import (
	"bitbucket.org/smeshkow/algorithms-go/item"
)

/***************************************************************************
*  Type aliases.
***************************************************************************/

// Item is an interface representing generic data transfer object.
type Item = item.Item

// Slice represents slice of instances of Item.
type Slice = item.Slice

// Orderable ...
type Orderable = item.Orderable

// Comparable ...
type Comparable = item.Comparable

/***************************************************************************
*  Helper sorting functions.
***************************************************************************/

// is v < w ?
func less(v, w Item) bool {
	return v.(Orderable).Less(w)
}

// exchange a[i] and a[j]
func exch(a Slice, i, j int) {
	a[i], a[j] = a[j], a[i]
}