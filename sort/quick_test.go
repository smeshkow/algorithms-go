package sort

import (
	"strings"
	"testing"

	"bitbucket.org/smeshkow/algorithms-go/item"
)

func TestQuick(t *testing.T)  {
	input := item.FromStrings(strings.Split("QUICKSORTEXAMPLE", ""))
	Quick(input)
	expected := "ACEEIKLMOPQRSTUX"
	actual := strings.Join(input.ToStrings(), "")
	if expected != actual {
		t.Errorf("expected order to be %s, but actual %s", expected, actual)
	}
}