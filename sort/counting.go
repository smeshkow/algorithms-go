package sort

// Counting ...
func Counting(a []int, n int) {
	counts := make([]int, n+1)

	// Take a count array to store the count of each unique object.
	for _, v := range a {
		counts[v]++
	}

	// Modify the count array such that each element at each index
	// stores the sum of previous counts.
	for i := 1; i < len(counts); i++ {
		counts[i] += counts[i-1]
	}

	aux := make([]int, len(a))
	// Output each object from the input sequence followed by
	// decreasing its count by 1.
	for _, v := range a {
		aux[counts[v] - 1] = v
		counts[v]--
	}

	// Update input
	copy(a, aux)
}
