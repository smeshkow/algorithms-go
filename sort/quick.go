package sort

import (
	"bitbucket.org/smeshkow/algorithms-go/random"
)

// Quick rearranges the array in ascending order, using the natural order.
func Quick(a Slice) {
	random.Shuffle(a)
	quickSort(a, 0, len(a) - 1)
}

// quicksort the subarray from a[lo] to a[hi]
func quickSort(a Slice, lo, hi int) {
	if hi <= lo {
		return
	}
	j := partition(a, lo, hi)
	quickSort(a, lo, j-1)
	quickSort(a, j+1, hi)
}

// partition the subarray a[lo..hi] so that a[lo..j-1] <= a[j] <= a[j+1..hi]
// and return the index j.
func partition(a Slice, lo, hi int) int {
	i := lo + 1
	j := hi
	v := a[lo]
	for {

		// find item on lo to swap
		for less(a[i], v) {
			if i == hi {
				break
			}
			i++
		}

		// find item on hi to swap
		for less(v, a[j]) {
			if j == lo {
				break // redundant since a[lo] acts as sentinel
			}
			j--
		}

		// check if pointers cross
		if i >= j {
			break
		}

		exch(a, i, j)
	}

	// put partitioning item v at a[j]
	exch(a, lo, j)

	// now, a[lo .. j-1] <= a[j] <= a[j+1 .. hi]
	return j
}
