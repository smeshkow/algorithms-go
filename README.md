# Algorithms Go

This is the attempt to provide full set of alforithms implementations in [Go](https://golang.org/) language for the algorithms and clients in the textbook
[Algorithms, 4th Edition](http://amzn.to/13VNJi7) by Robert Sedgewick and Kevin Wayne.

Running `algs4 random -n=20 -lo=1 -hi=50 | algs4 search -s=binary -k=15` will produce something like this:
```bash
search in sorted list: [1 3 4 11 11 15 16 16 17 21 23 24 26 31 33 35 40 43 45 48]
key 15 found at index 5
```

# Disclaimer

This is not official version, which does not mean that it can not become one in the future.

## Installation

### Binary

* download latest release for corrseponding OS (Darwin or Linux) from [Downloads](https://bitbucket.org/smeshkow/algorithms-go/downloads)
* make binary executable: `chmod +x <binary>`
* put executable binary under your bin directory, e.g. (assuming `~/bin` is in your PATH): `mv <binary> $HOME/bin/<binary>`.

### Go dependency

```bash
go get -u bitbucket.org/smeshkow/algorithms-go/...
```

# Check-list

### Collections

- [x] Linked List
- [x] Queue (slice based)
- [x] Queue (linked list based)
- [x] Stack (slice based)
- [x] Stack (linked list based)
- [x] Bag (slice based)
- [x] Bag (linked list based)
- [x] Union Find (weighted quick union based)

...todo.

### Garph

- [x] Graph (adjacent lists based)
- [ ] Depth-first search (DFS)
- [ ] Breadth-first search (BFS)

### Sort

- [ ] Selection
- [ ] Insertion
- [x] Quick
- [x] Quick 3 way
- [x] Merge
- [x] Counting

...todo.

### Search

- [x] Binary

...todo.


## Changelog

See [CHANGELOG.md](https://bitbucket.org/smeshkow/algorithms-go/raw/master/CHANGELOG.md)

## Contributing

See [CONTRIBUTING.md](https://bitbucket.org/smeshkow/algorithms-go/raw/master/CONTRIBUTING.md)