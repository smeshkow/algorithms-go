package graph

import (
	"testing"

	"bitbucket.org/smeshkow/algorithms-go/collections"
)

func TestVertices(t *testing.T) {
	graph := New(10)
	if graph.V() != 10 {
		t.Error("expected 10 vertices in the graph")
	}
}

func TestAdjacentVertices(t *testing.T) {
	graph := New(10)

	graph.AddEdge(0, 1)
	graph.AddEdge(3, 7)
	graph.AddEdge(1, 9)

	adj := graph.Adj(1)

	bag, ok := adj.(collections.Bag)

	if !ok {
		t.Error("expected #Adj to return bag")
	}

	if bag.Size() != 2 {
		t.Error("expected #Adj of 1 to be of size 2")
	}

	bag.Iterator().Iterate(func (item Item)  {
		if item.GetInt() != 0 && item.GetInt() != 9 {
			t.Error("expected #Adj of 1 to be 0 & 9")
		}
	})
}
