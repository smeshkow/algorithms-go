package graph

import (
	"fmt"
)

// DFS ...
type DFS struct {
	marked []bool // marked[v] = is there an s-v path?
	count  int    // number of vertices connected to s
}

// NewDFS ...
func NewDFS(g Graph, s int) *DFS {
	dfs := DFS{marked: make([]bool, g.V())}
	dfs.validateVertex(s)
	dfs.dfs(g, s)
	return &dfs
}

// depth first search from v
func (dfs *DFS) dfs(g Graph, v int) {
	dfs.count++
	dfs.marked[v] = true
	g.Adj(v).Iterator().Iterate(func(item Item){
		if !dfs.marked[item.GetInt()] {
			dfs.dfs(g, item.GetInt());
		}
	})
}


// Marked - is there a path between the source vertex s and vertex v?
// Receives v the vertex.
// returns true if there is a path, false otherwise.
// panics unless 0 <= v < V.
func (dfs *DFS) Marked(v int) bool {
	dfs.validateVertex(v)
	return dfs.marked[v]
}

 // Count returns the number of vertices connected to the source vertex s.
func (dfs *DFS) Count() int {
	return dfs.count;
}

// panics unless 0 <= v < V
func (dfs *DFS) validateVertex(v int) {
	markedLen := len(dfs.marked)
	if v < 0 || v >= markedLen {
		panic(fmt.Sprintf("vertex %d is not between 0 and %d", v, markedLen-1))
	}
}
