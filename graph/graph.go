package graph

import (
	"fmt"
	"strings"

	"bitbucket.org/smeshkow/algorithms-go/collections"
	"bitbucket.org/smeshkow/algorithms-go/inout"
	"bitbucket.org/smeshkow/algorithms-go/item"
	"bitbucket.org/smeshkow/algorithms-go/util"
)

// Item ...
type Item = item.Item

// Iterable ...
type Iterable = collections.Iterable

// Bag ...
type Bag = collections.Bag

// In ...
type In = inout.In

// Graph represents an undirected graph of vertices named 0 through V – 1.
// It supports the following two primary operations: add an edge to the graph,
// iterate over all of the vertices adjacent to a vertex. It also provides
// methods for returning the number of vertices V and the number
// of edges E. Parallel edges and self-loops are permitted.
// By convention, a self-loop v-v appears in the
// adjacency list of v twice and contributes two to the degree
// of v.
type Graph interface {
	V() int             // number of vertices
	E() int             // number of edges
	AddEdge(v, w int)   // add edge v-w to this graph
	Adj(v int) Iterable // vertices adjacent to v
	String() string     // string representation
}

// AdjListGraph implementation uses an adjacency-lists representation, which
// is a vertex-indexed array of Bag objects.
// All operations take constant time (in the worst case) except
// iterating over the vertices adjacent to a given vertex, which takes
// time proportional to the number of such vertices.
//
// For additional documentation, see <a href="https://algs4.cs.princeton.edu/41graph">Section 4.1</a>
// of "Algorithms, 4th Edition" by Robert Sedgewick and Kevin Wayne.
type AdjListGraph struct {
	v   int   // number of vertices
	e   int   // number of edges
	adj []Bag // adjacency lists
}

// New Initializes an empty graph with V vertices and 0 edges.
// param V the number of vertices.
func New(v int) Graph {
	adj := make([]Bag, v)
	for i := 0; i < v; i++ {
		adj[i] = collections.NewLinkedListBag()
	}
	return &AdjListGraph{
		v:   v,
		adj: adj,
	}
}

// NewFromIn Initializes a graph from the specified input stream.
// The format is the number of vertices V,
// followed by the number of edges E,
// followed by E pairs of vertices, with each entry separated by whitespace.
func NewFromIn(in In) Graph {
	defer in.Close()
	v := in.ReadInt()
	if v < 0 {
		panic("v < 0")
	}
	adj := make([]Bag, v)
	for i := 0; i < v; i++ {
		adj[i] = collections.NewLinkedListBag()
	}
	e := in.ReadInt()
	if e < 0 {
		panic("e < 0")
	}
	graph := &AdjListGraph{
		v:   v,
		adj: adj,
		e:   e,
	}
	for i := 0; i < e; i++ {
		edge := in.ReadString()
		if edge == "" {
			continue
		}
		split := strings.Split(edge, " ")
		v1 := inout.ParseInt(split[0])
		v2 := inout.ParseInt(split[1])
		graph.AddEdge(v1, v2)
	}
	return graph
}

// V returns the number of vertices in this graph.
func (g *AdjListGraph) V() int {
	return g.v
}

// E returns the number of edges in this graph.
func (g *AdjListGraph) E() int {
	return g.e
}

// AddEdge adds the undirected edge v-w to this graph.
func (g *AdjListGraph) AddEdge(v, w int) {
	g.validateVertex(v)
	g.validateVertex(w)
	g.e++
	g.adj[v].Add(item.NewInt(w))
	g.adj[w].Add(item.NewInt(v))
}

// Adj returns the vertices adjacent to vertex v.
func (g *AdjListGraph) Adj(v int) Iterable {
	g.validateVertex(v)
	return g.adj[v]
}

// Degree returns the degree of vertex v.
func (g *AdjListGraph) Degree(v int) int {
	g.validateVertex(v)
	return g.adj[v].Size()
}

// String ...
func (g *AdjListGraph) String() string {
	var s strings.Builder
	var err error
	_, err = s.WriteString(fmt.Sprintf("%d vertices, %d edges \n", g.v, g.e))
	util.Must(err)
	for v := 0; v < g.v; v++ {
		_, err = s.WriteString(fmt.Sprintf("%d: ", v))
		util.Must(err)
		g.adj[v].Iterator().Iterate(func(item Item) {
			_, err = s.WriteString(fmt.Sprintf("%d ", item.GetInt()))
			util.Must(err)
		})
		_, err = s.WriteString("\n")
		util.Must(err)
	}
	return s.String()
}

// panic unless 0 <= v < V
func (g *AdjListGraph) validateVertex(v int) {
	if v < 0 || v >= g.v {
		panic(fmt.Sprintf("vertex %d is not between 0 and %d", v, g.v-1))
	}
}
