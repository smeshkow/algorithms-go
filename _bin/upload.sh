#!/bin/bash

user=$USER
password=$PASSWORD
repo="$BITBUCKET_REPO_OWNER/$BITBUCKET_REPO_SLUG"
file=$1

curl --insecure               `# make tls cert validation optional, read this if you need it (https://curl.haxx.se/docs/sslcerts.html) ` \
     --progress-bar           `# print the progress visually                                                                          ` \
     --location               `# follow redirects if we are told so                                                                 ` \
     --fail                   `# ensure that we are not succeeding when the server replies okay but with an error code             ` \
     --write-out %{http_code} `# write the returned error code to stdout, we will redirect it later to a file for parsing         ` \
     --user "$user:$password"       `# basic auth so that it lets us in                                                                ` \
     --form files=@"$file" "https://api.bitbucket.org/2.0/repositories/$repo/downloads"
