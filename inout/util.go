package inout

import (
	"strconv"

	"bitbucket.org/smeshkow/algorithms-go/util"
)

// ParseInt parses given string to int.
func ParseInt(s string) int {
	n, err := strconv.ParseInt(s, 10, 64)
	util.Must(err)
	return int(n)
}