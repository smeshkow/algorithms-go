package item

import (
	"fmt"
	"strings"
)

// SimpleItem is a basic and most simple implementation of Item and ComparableItem interfaces.
type SimpleItem struct {
	intVal     int
	float64Val float64
	stringVal  string
	by         By
	cmp        Cmp
}

// FromStrings ...
func FromStrings(vals []string) Slice {
	items := make([]Item, len(vals))
	for i, v := range vals {
		items[i] = NewString(v).(*SimpleItem).WithBy(ByString).(*SimpleItem).WithCmp(CmpString).(Item)
	}
	return items
}

// FromInts ...
func FromInts(vals []int) Slice {
	items := make([]Item, len(vals))
	for i, v := range vals {
		items[i] = NewInt(v).(*SimpleItem).WithBy(ByInt).(*SimpleItem).WithCmp(CmpInt).(Item)
	}
	return items
}

// New ...
func New() Item {
	return &SimpleItem{}
}

// NewInt ...
func NewInt(val int) Item {
	i := &SimpleItem{intVal: val}
	return i.WithBy(ByInt).(Item)
}

// NewString ...
func NewString(val string) Item {
	i := &SimpleItem{stringVal: val}
	return i.WithBy(ByString).(Item)
}

// NewFloat64 ...
func NewFloat64(val float64) Item {
	i := &SimpleItem{float64Val: val}
	return i.WithBy(ByFloat64).(Item)
}

// WithBy ...
func (i *SimpleItem) WithBy(by By) Orderable {
	i.by = by
	return i
}

// WithCmp ...
func (i *SimpleItem) WithCmp(cmp Cmp) Comparable {
	i.cmp = cmp
	return i
}

// GetInt ...
func (i *SimpleItem) GetInt() int {
	return i.intVal
}

// GetFloat64 ...
func (i *SimpleItem) GetFloat64() float64 {
	return i.float64Val
}

// GetString ...
func (i *SimpleItem) GetString() string {
	return i.stringVal
}

// SetInt ...
func (i *SimpleItem) SetInt(val int) {
	i.intVal = val
}

// SetFloat64 ...
func (i *SimpleItem) SetFloat64(val float64) {
	i.float64Val = val
}

// SetString ...
func (i *SimpleItem) SetString(val string) {
	i.stringVal = val
}

// Equals ...
func (i *SimpleItem) Equals(item Item) bool {
	return i.GetInt() == item.GetInt() &&
		i.GetFloat64() == item.GetFloat64() &&
		i.GetString() == item.GetString()
}

// Less ...
func (i *SimpleItem) Less(item Item) bool {
	return i.by(i, item)
}

// LessBy ...
func (i *SimpleItem) LessBy(item Item, by By) bool {
	return by(i, item)
}

// Compare ...
func (i *SimpleItem) Compare(item Item) int {
	return i.cmp(i, item)
}

// CompareTo ...
func (i *SimpleItem) CompareTo(item Item, cmp Cmp) int {
	return cmp(i, item)
}

// String ...
func (i *SimpleItem) String() string {
	var out []string
	out = append(out, fmt.Sprintf("[%d]", i.GetInt()))
	out = append(out, fmt.Sprintf("[%.4f]", i.GetFloat64()))
	if i.GetString() != "" {
		out = append(out, fmt.Sprintf("[%s]", i.GetString()))
	}
	return strings.Join(out, " ")
}
