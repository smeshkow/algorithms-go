package item

import (
	"fmt"
	"strings"
	"math"
	"sort"
)

// Sort fucntions
var (
	ByInt = func(i1, i2 Item) bool {
		return i1.GetInt() < i2.GetInt()
	}

	ByFloat64 = func(i1, i2 Item) bool {
		return i1.GetFloat64() < i2.GetFloat64()
	}

	ByString = func(i1, i2 Item) bool {
		return i1.GetString() < i2.GetString()
	}

	CmpInt = func(i1, i2 Item) int {
		return i1.GetInt() - i2.GetInt()
	}

	CmpFloat64 = func(i1, i2 Item) int {
		return int(math.Ceil(i1.GetFloat64() - i2.GetFloat64()))
	}

	CmpString = func(i1, i2 Item) int {
		return strings.Compare(i1.GetString(), i2.GetString())
	}
)

// Item is an interface representing generic data transfer object.
type Item interface {
	// Getters
	GetInt() int
	GetFloat64() float64
	GetString() string

	// Setters
	SetInt(val int)
	SetFloat64(val float64)
	SetString(val string)

	// Misc
	Equals(item Item) bool
}

// Orderable ...
type Orderable interface {
	WithBy(by By) Orderable
	Less(item Item) bool
	LessBy(item Item, by By) bool
}

// OrderableItem ...
type OrderableItem interface {
	Item
	Orderable
}

// Comparable ...
type Comparable interface {
	WithCmp(cmp Cmp) Comparable
	Compare(item Item) int
	CompareTo(item Item, cmp Cmp) int
}

// ComparableItem ...
type ComparableItem interface {
	Item
	Comparable
}

// Sortable ...
type Sortable interface {
	Orderable
	Comparable
}

// SortableItem ...
type SortableItem interface {
	Item
	Orderable
	Comparable
}

// Slice represents slice of instances of Item.
type Slice []Item

// Equals ...
func (s Slice) Equals(slice Slice) bool {
	if slice == nil || len(s) != len(slice) {
		return false
	}
	for i, v := range s {
		if !v.Equals(slice[i]) {
			return false
		}
	}
	return true
}

// ToStrings ...
func (s Slice) ToStrings() []string {
	vals := make([]string, len(s))
	for i, v := range s {
		vals[i] = v.GetString()
	}
	return vals
}

// ToInts ...
func (s Slice) ToInts() []int {
	vals := make([]int, len(s))
	for i, v := range s {
		vals[i] = v.GetInt()
	}
	return vals
}

// String ...
func (s Slice) String() string {
	var sb strings.Builder
	var err error
	for _, v := range s {
		if sb.Len() == 0 {
			_, err = sb.WriteString(fmt.Sprintf("%s", v))
		} else {
			_, err = sb.WriteString(fmt.Sprintf(", %s", v))
		}
		if err != nil {
			panic(err)
		}
	}
	return sb.String()
}

// ------------------------------------------ Compare ------------------------------------------

// Cmp is the type of a "compare" function that defines the ordering of its Item arguments.
type Cmp func(i1, i2 Item) int

// ------------------------------------------ Sort ------------------------------------------

// By is the type of a "less" function that defines the ordering of its Item arguments.
type By func(i1, i2 Item) bool

// Sort is a method on the function type, By, that sorts the argument slice according to the function.
func (by By) Sort(items []Item) {
	sorter := &itemSorter{
		items: items,
		by:    by, // The Sort method's receiver is the function (closure) that defines the sort order.
	}
	sort.Sort(sorter)
}

// itemSorter joins a By function and a slice of Items to be sorted.
type itemSorter struct {
	items []Item
	by    func(p1, p2 Item) bool // Closure used in the Less method.
}

// Len is part of sort.Interface.
func (s *itemSorter) Len() int {
	return len(s.items)
}

// Swap is part of sort.Interface.
func (s *itemSorter) Swap(i, j int) {
	s.items[i], s.items[j] = s.items[j], s.items[i]
}

// Less is part of sort.Interface. It is implemented by calling the "by" closure in the sorter.
func (s *itemSorter) Less(i, j int) bool {
	return s.by(s.items[i], s.items[j])
}
