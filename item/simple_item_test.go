package item

import (
	"testing"
)

func TestSimpleItem_Equals(t *testing.T) {
	item1 := &SimpleItem{
		intVal:     1,
		float64Val: 2.0,
		stringVal:  "3",
	}

	item2 := &SimpleItem{}
	item2.SetInt(1)
	item2.SetFloat64(2.0)
	item2.SetString("3")

	if !item1.Equals(item2) {
		t.Error("item1 is not equal to item2")
	}

	if !item2.Equals(item1) {
		t.Error("item2 is not equal to item1")
	}
}
