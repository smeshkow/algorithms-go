package item

import (
	"testing"
)

func TestDataItem_Equals(t *testing.T) {
	item1 := &DataItem{
		data: Data{
			"foo": map[int]interface{}{IntVal: 1},
			"bar": map[int]interface{}{Float64Val: 2.0},
			"bee": map[int]interface{}{StringVal: "3"},
		},
	}

	item2 := &DataItem{}
	item2.SetPropValue("foo", 1)
	item2.SetPropValue("bar", 2.0)
	item2.SetPropValue("bee", "3")

	if !item1.Equals(item2) {
		t.Error("item1 is not equal to item2")
	}

	if !item2.Equals(item1) {
		t.Error("item2 is not equal to item1")
	}
}