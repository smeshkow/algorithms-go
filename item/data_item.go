package item

import (
	"fmt"
)

// Field codes of GenericItem.
const (
	IntVal = iota
	Float64Val
	StringVal
	StructVal

	intProp     = "int"
	float64Prop = "float64"
	stringProp  = "string"
)

// Data stores internals of GenericItem.
type Data map[string]map[int]interface{}

// GenericItem is an interface representing generic data transfer model.
type GenericItem interface {
	SetPropValue(name string, val interface{})
	GetIntProp(name string) int
	GetFloat64Prop(name string) float64
	GetStringProp(name string) string
}

// DataItem implements Item interface based on internal map of fields - Data.
type DataItem struct {
	data Data
	by   By
}

// NewGeneric ...
func NewGeneric() *DataItem {
	return &DataItem{}
}

// NewDataItem ...
func NewDataItem(name string, val interface{}) *DataItem {
	di := &DataItem{}
	di.SetPropValue(name, val)
	return di
}

// WithBy ...
func (i *DataItem) WithBy(by By) Orderable {
	i.by = by
	return i
}

// GetInt ...
func (i *DataItem) GetInt() int {
	return i.GetIntProp(intProp)
}

// GetFloat64 ...
func (i *DataItem) GetFloat64() float64 {
	return i.GetFloat64Prop(float64Prop)
}

// GetString ...
func (i *DataItem) GetString() string {
	return i.GetStringProp(stringProp)
}

// SetInt ...
func (i *DataItem) SetInt(val int) {
	i.SetPropValue(intProp, val)
}

// SetFloat64 ...
func (i *DataItem) SetFloat64(val float64) {
	i.SetPropValue(float64Prop, val)
}

// SetString ...
func (i *DataItem) SetString(val string) {
	i.SetPropValue(stringProp, val)
}

// Equals ...
func (i *DataItem) Equals(item Item) bool {
	dataItem, ok := item.(*DataItem)
	if !ok {
		return false
	}

	for name, prop := range i.data {
		var dataProp map[int]interface{}
		dataProp, ok = dataItem.data[name]
		if !ok {
			fmt.Printf("%s property not found in item\n", name)
			return false
		}
		for key, val := range prop {
			dataVal, okVal := dataProp[key]
			if !okVal {
				fmt.Printf("%d value not found in item's property\n", key)
				return false
			}
			if val != dataVal {
				fmt.Printf("%v != %v\n", val, dataVal)
				return false
			}
		}
	}

	return true
}

// Less ...
func (i *DataItem) Less(item Item) bool {
	return i.by(i, item)
}

// LessBy ...
func (i *DataItem) LessBy(item Item, by By) bool {
	return by(i, item)
}

// SetPropValue sets value of provided property's name.
func (i *DataItem) SetPropValue(name string, val interface{}) {
	if i.data == nil {
		i.data = Data(make(map[string]map[int]interface{}))
	}

	var property map[int]interface{}
	var ok bool
	if property, ok = i.data[name]; !ok {
		property = make(map[int]interface{})
		i.data[name] = property
	}

	if intVal, ok := val.(int); ok {
		property[IntVal] = intVal
	} else if float64Val, ok := val.(float64); ok {
		property[Float64Val] = float64Val
	} else if stringVal, ok := val.(string); ok {
		property[StringVal] = stringVal
	} else {
		panic(fmt.Sprintf("unknown type of: %v", val))
	}
}

// GetIntProp returns int value of provided property name.
func (i *DataItem) GetIntProp(name string) int {
	var property map[int]interface{}
	var ok bool
	if property, ok = i.data[name]; !ok {
		return 0
	}
	return property[IntVal].(int)
}

// GetFloat64Prop returns float64 value of provided property name.
func (i *DataItem) GetFloat64Prop(name string) float64 {
	var property map[int]interface{}
	var ok bool
	if property, ok = i.data[name]; !ok {
		return 0
	}
	return property[Float64Val].(float64)
}

// GetStringProp returns string value of provided property name.
func (i *DataItem) GetStringProp(name string) string {
	var property map[int]interface{}
	var ok bool
	if property, ok = i.data[name]; !ok {
		return ""
	}
	return property[StringVal].(string)
}
