package search

import (
	"fmt"
	"sort"

	"bitbucket.org/smeshkow/algorithms-go/inout"
)

// Returns the index of the specified key in the specified array or -1 if not present.
// a - the array of integers, must be sorted in ascending order
// key - the search key
func indexOf(key int, a []int) int {
	lo := 0
	hi := len(a) - 1
	for lo <= hi {
		// Key is in a[lo..hi] or not present.
		mid := lo + (hi-lo)/2
		if key < a[mid] {
			hi = mid - 1
		} else if key > a[mid] {
			lo = mid + 1
		} else {
			return mid
		}
	}
	return -1
}

func indexOfRecursive(key, lo, hi, depth int, a []int) int {
	if lo > hi {
		return -1
	}

	for i:=0;i<depth;i++ {
		print(" - ")
	}
	fmt.Printf("lo = %d, hi = %d\n", lo, hi)

	// Key is in a[lo..hi] or not present.
	mid := lo + (hi-lo)/2
	if key < a[mid] {
		return indexOfRecursive(key, lo, mid-1, depth+1, a)
	} else if key > a[mid] {
		return indexOfRecursive(key, mid+1, hi, depth+1, a)
	} else {
		return mid
	}
}

// BinarySearch ...
func BinarySearch(name string, key int) {
	in := inout.NewIn(name)
	whitelist := in.ReadAllInts()
	sort.Ints(whitelist)

	fmt.Printf("search in sorted list: %v\n", whitelist)

	index := indexOf(key, whitelist)
	if index == -1 {
		fmt.Printf("key %d not found in %s\n", key, name)
		return
	}

	fmt.Printf("key %d found at index %d\n", key, index)
}

// BinarySearchRecursive ...
func BinarySearchRecursive(name string, key int) {
	in := inout.NewIn(name)
	whitelist := in.ReadAllInts()
	sort.Ints(whitelist)

	fmt.Printf("search in sorted list: %v\n", whitelist)

	index := indexOfRecursive(key, 0, len(whitelist) - 1, 0, whitelist)
	if index == -1 {
		fmt.Printf("key %d not found in %s\n", key, name)
		return
	}

	fmt.Printf("key %d found at index %d\n", key, index)
}